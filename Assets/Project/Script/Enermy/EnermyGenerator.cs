﻿using System.Collections;
using UnityEngine;
using PlayerController = Project.Script.Player.PlayerController;

namespace Project.Script.Enermy
{
    public class EnermyGenerator : MonoBehaviour
    {
        [SerializeField] private GameObject enermyPrefab;
        [SerializeField] private Vector2 spawnValues;
        [SerializeField] private int enermyCount;// meteo ต่อรอบ
        [SerializeField] private int enermyCountPlus;// meteo ทุกๆ รอบ จะเพิ่ม meteo ขึ้น
        [SerializeField] private float spawnWait;
        [SerializeField] private float waveWait;
        [SerializeField] private float startWait;

        public PlayerController playerController;
        
        private void Start()
        {
            StartCoroutine(SpawnWaves());
        }
        
        IEnumerator SpawnWaves()
        {
            yield return new WaitForSeconds(startWait);
        
            while (true)
            {
                for (int i = 0; i < enermyCount; i++) 
                {
                    Vector2 spawnPosition = new Vector2(Random.Range(-spawnValues.x, spawnValues.x), Random.Range(spawnValues.y, spawnValues.y +5));
                    Quaternion spawnRotation = Quaternion.identity;

                    Instantiate(enermyPrefab, spawnPosition, spawnRotation);

                    yield return new WaitForSeconds(spawnWait);
                }
                
                enermyCount += enermyCountPlus;
                
                yield return new WaitForSeconds(waveWait);
            }
            // ReSharper disable once IteratorNeverReturns
        }
    }
}
