﻿using UnityEngine;
using UnityEngine.SceneManagement;
using PlayerController = Project.Script.Player.PlayerController;

namespace Project.Script.Enermy
{
    public class EnermyMove : MonoBehaviour
    {
        [SerializeField] private float enemySpeed = 5;

        [SerializeField] private GameObject enermyGun;
        [SerializeField] private Transform enermyGunSpawn;
        [SerializeField] private float fireRate;
        [SerializeField] private float nextFire;
        
        [SerializeField] private float moveAmount;
        private bool isMoveLeft;
        private float moveMin;
        private float moveMax;
        
        [SerializeField] private AudioClip sound;
        [SerializeField] private float volume = 0.2f;
        
        private Renderer playerRender;
        private Renderer enemyRender;

        public PlayerController playerController;
        
        private void Start()
        {
            playerRender = GetComponent<Renderer>();
            
            playerController = GameObject.Find("ShipPlayer").GetComponent<PlayerController>();

            isMoveLeft = true;
            var position = transform.position;
            moveMin = position.x - moveAmount;
            moveMax = position.x + moveAmount;
        }
        
        private void Awake()
        {
            enemyRender = GetComponent<Renderer>();
        }
        
        private void Update()
        {
            OnShooting();
        }
        
        void FixedUpdate()
        {
            if (Time.timeScale == 0)
            {
                return;
            }
            if (isMoveLeft)
            {
                if (transform.position.x > moveMin)
                {
                    transform.Translate(-0.01f * enemySpeed * Time.deltaTime, 0, 0);
                }
                else
                {
                    isMoveLeft = false;
                }
            }
            else
            {
                if (transform.position.x < moveMax)
                {
                    transform.Translate(0.01f * enemySpeed * Time.deltaTime, 0, 0);
                }
                else
                {
                    isMoveLeft = true;
                }
            }
        }
        
        private void OnDrawGizmos()
        {
            CollisionDebug();
        }
        
        private void CollisionDebug()
        {
            if (enemyRender != null && playerRender != null)
            {
                if (intersectAaBb(enemyRender.bounds, playerRender.bounds))
                {
                    Gizmos.color = Color.red;
                }
                else
                {
                    Gizmos.color = Color.white;
                }

                var bounds = enemyRender.bounds;
                Gizmos.DrawWireCube(bounds.center, 2 * bounds.extents);
                var bounds1 = playerRender.bounds;
                Gizmos.DrawWireCube(bounds1.center, 2 * bounds1.extents);
            }
        }
        
        private bool intersectAaBb(Bounds a, Bounds b)
        {
            return (a.min.x <= b.max.x && a.max.x >= b.min.x) && (a.min.y <= b.max.y && a.max.y >= b.min.y);
        }
        
        void OnShooting()
        {
            if (Time.time > nextFire)
            {
                nextFire = Time.time + fireRate;
                Instantiate(enermyGun, enermyGunSpawn.position, enermyGunSpawn.rotation);
            }
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                Time.timeScale = 0;
                SceneManager.LoadScene("GameOver");
            }
            else if (other.CompareTag("GunType"))
            {
                playerController.AddScore(5);
                
                Destroy(other.gameObject);
                Destroy(gameObject);
                
                if (!(Camera.main is null)) AudioSource.PlayClipAtPoint(sound, Camera.main.transform.position, volume);
            }
        }
    }
}

