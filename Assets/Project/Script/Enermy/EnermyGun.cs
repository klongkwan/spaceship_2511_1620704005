﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Project.Script.Enermy
{
    public class EnermyGun : MonoBehaviour
    {
        [SerializeField] private float speed;
        
        private Rigidbody2D rb;
        
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            rb.velocity = transform.forward * speed * Time.deltaTime;
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                Destroy(gameObject);
                Time.timeScale = 0;
                SceneManager.LoadScene("GameOver");
            }
        }
    }
}
