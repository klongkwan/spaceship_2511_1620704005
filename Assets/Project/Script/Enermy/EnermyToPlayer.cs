﻿using UnityEngine;
using UnityEngine.SceneManagement;
using PlayerController = Project.Script.Player.PlayerController;

namespace Project.Script.Enermy
{
    public class EnermyToPlayer : MonoBehaviour
    {
        [SerializeField] private AudioClip sound;
        [SerializeField] private float volume = 0.2f;
        
        [SerializeField] private float speed = 5f;
        [SerializeField] private GameObject targetPlayer;
        
        private Renderer playerRender;
        private Renderer enemyRender;
        
        private Rigidbody2D rb;

        public PlayerController playerController;
        
        private void Start()
        {
            playerRender = GetComponent<Renderer>();
            
            rb = GetComponent<Rigidbody2D>();
            
            targetPlayer = GameObject.FindGameObjectWithTag("Player");

            playerController = GameObject.Find("ShipPlayer").GetComponent<PlayerController>();

        }
        
        private void Awake()
        {
            enemyRender = GetComponent<Renderer>();
        }
        
        void FixedUpdate()
        {
            MoveToPlayer();
        }
        
        private void MoveToPlayer()
        {
            float moveX = 0f;
            if (Mathf.Round(targetPlayer.transform.position.x) > Mathf.Round(transform.position.x))
            {
                moveX = 1f;
            }
            else if (Mathf.Round(targetPlayer.transform.position.x) < Mathf.Round(transform.position.x))
            {
                moveX = -1f;
            }
            rb.velocity = new Vector2(moveX,-1f) * speed;
        }
        
        private void OnDrawGizmos()
        {
            CollisionDebug();
        }
        
        private void CollisionDebug()
        {
            if (enemyRender != null && playerRender != null)
            {
                if (intersectAaBb(enemyRender.bounds, playerRender.bounds))
                {
                    Gizmos.color = Color.red;
                }
                else
                {
                    Gizmos.color = Color.white;
                }
                var bounds = enemyRender.bounds;
                Gizmos.DrawWireCube(bounds.center, 2 * bounds.extents);
                var bounds1 = playerRender.bounds;
                Gizmos.DrawWireCube(bounds1.center, 2 * bounds1.extents);
            }
        }
        
        private bool intersectAaBb(Bounds a, Bounds b)
        {
            return (a.min.x <= b.max.x && a.max.x >= b.min.x) && (a.min.y <= b.max.y && a.max.y >= b.min.y);
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                Time.timeScale = 0;
                SceneManager.LoadScene("GameOver");
            }
            else if (other.CompareTag("GunType"))
            {
                playerController.AddScore(5);
                
                Destroy(other.gameObject);
                Destroy(gameObject);
                
                if (!(Camera.main is null)) AudioSource.PlayClipAtPoint(sound, Camera.main.transform.position, volume);
            }
        }
    }
}

