﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using PlayerController = Project.Script.Player.PlayerController;

namespace Project.Script.Meteo
{
    public class Meteo : MonoBehaviour
    {
        [SerializeField] private AudioClip sound;
        [SerializeField] private float volume = 0.2f;
        
        public PlayerController playerController;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("GunType"))
            {
                playerController.AddScore(3);

                Destroy(other.gameObject);
                Destroy(gameObject);

                if (!(Camera.main is null)) AudioSource.PlayClipAtPoint(sound, Camera.main.transform.position, volume);
            }
            else if (other.CompareTag("Player"))
            {
                Time.timeScale = 0;
                SceneManager.LoadScene("GameOver");
            }
        }
    }
}
