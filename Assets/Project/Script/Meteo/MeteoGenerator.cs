﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Project.Script.Meteo
{
    public class MeteoGenerator : MonoBehaviour
    {
        [SerializeField] private GameObject meteoPrefab;
        [SerializeField] private Vector2 spawnValues;
        [SerializeField] private int meteoCount;// meteo ต่อรอบ
        [SerializeField] private int meteoCountPlus;// meteo ทุกๆ รอบ จะเพิ่ม meteo ขึ้น
        [SerializeField] private float spawnWait;
        [SerializeField] private float waveWait;
        [SerializeField] private float startWait;

        public Player.PlayerController playerController;
        
        private void Start()
        {
            StartCoroutine(SpawnWaves());
        }
        IEnumerator SpawnWaves()
        {
            yield return new WaitForSeconds(startWait);
        
            while (true)
            {
                for (int i = 0; i < meteoCount; i++) 
                {
                    Vector2 spawnPosition = new Vector2(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y);
                    Quaternion spawnRotation = Quaternion.identity;
                    
                    var meteoSpawn = Instantiate(meteoPrefab, spawnPosition, spawnRotation);
                    meteoSpawn.GetComponent<Meteo>().playerController = playerController;
            
                    yield return new WaitForSeconds(spawnWait);
                }

                meteoCount += meteoCountPlus;
                
                yield return new WaitForSeconds(waveWait);
            }
            // ReSharper disable once IteratorNeverReturns
        }
    }
}
