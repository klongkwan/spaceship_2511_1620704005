﻿using UnityEngine;
using UnityEngine.UI;

namespace Project.Script.Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float speed;
        
        [SerializeField] private float fireRate;
        [SerializeField] private float nextFire;
        [SerializeField] private GameObject gunLaser;
        [SerializeField] private Transform gunSpawn;
        [SerializeField] private Transform gunSpawn2; 
        [SerializeField] private Transform gunSpawn3;
        
        [SerializeField] private float missileFireRate;
        [SerializeField] private float missileNextFire;
        [SerializeField] private GameObject gunMissile;
        [SerializeField] private Transform missileSpawn; 
        [SerializeField] private Transform missileSpawn2;
        
        [SerializeField] private float xBox;
        [SerializeField] private float yBox;
        
        [SerializeField] private Text scoreText;
        private int score;

        private Rigidbody2D rb;
        private Vector2 movement;
        private float moveVertical;
        private float moveHorizontal;
        
        void Start()
        {
            Application.targetFrameRate = 60;
            
            rb = GetComponent<Rigidbody2D>();
            
            score = 0;
            UpdateScore();
        }
        
        private void Update()
        {
            if (score > 100 && Input.GetButton("Fire1") && Time.time > nextFire)
            {
                AttLv4();
            }
            else if (score > 50 && Input.GetButton("Fire1") && Time.time > nextFire)
            {
                AttLv3();
            }
            else if (score > 25 && Input.GetButton("Fire1") && Time.time > nextFire) 
            {
                AttLv2();
            }
            else if (Input.GetButton("Fire1") && Time.time > nextFire)
            {
                AttLv1();
            }
        }
        
        void FixedUpdate()
        {
            moveHorizontal = Input.GetAxis("Horizontal");
            moveVertical = Input.GetAxis("Vertical");
            movement = new Vector2(moveHorizontal, moveVertical);
            rb.velocity = movement * speed * Time.deltaTime;
        
            // ขอบเขตการเครื่อนที่
            rb.position = new Vector2(
                Mathf.Clamp(rb.position.x, -xBox, xBox),
                Mathf.Clamp(rb.position.y, -yBox, yBox)
            );
        }

        void UpdateScore()
        {
            scoreText.text = "Score :" + score.ToString();
        }

        public void AddScore(int newScore)
        {
            score += newScore;
            UpdateScore();
        }

        void AttLv1()
        {
            nextFire = Time.time + fireRate;
            Instantiate(gunLaser, gunSpawn.position, gunSpawn.rotation);

            GetComponent<AudioSource>().Play();
        }
        
        void AttLv2()
        {
            nextFire = Time.time + fireRate;
            Instantiate(gunLaser, gunSpawn2.position, gunSpawn2.rotation);
            Instantiate(gunLaser, gunSpawn3.position, gunSpawn3.rotation);
                
            GetComponent<AudioSource>().Play();
        }
        
        void AttLv3()
        {
            nextFire = Time.time + fireRate;
            Instantiate(gunLaser, gunSpawn.position, gunSpawn.rotation);
            Instantiate(gunLaser, gunSpawn2.position, gunSpawn2.rotation);
            Instantiate(gunLaser, gunSpawn3.position, gunSpawn3.rotation);
                
            GetComponent<AudioSource>().Play();
        }
        
        void AttLv4()
        {
            nextFire = Time.time + fireRate;
            Instantiate(gunLaser, gunSpawn.position, gunSpawn.rotation);
            Instantiate(gunLaser, gunSpawn2.position, gunSpawn2.rotation);
            Instantiate(gunLaser, gunSpawn3.position, gunSpawn3.rotation);


            if (Time.time > missileNextFire)
            {
                missileNextFire = Time.time + missileFireRate;
                Instantiate(gunMissile, missileSpawn.position, missileSpawn.rotation);
                Instantiate(gunMissile, missileSpawn2.position, missileSpawn2.rotation);
            }
            
            GetComponent<AudioSource>().Play();
        }
    }
}