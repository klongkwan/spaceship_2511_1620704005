﻿using System;
using UnityEngine;

namespace Project.Script.Player
{
    public class GunLaserMove : MonoBehaviour
    {
        [SerializeField] private float speed;
        private Rigidbody2D rb;
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
        }

        private void FixedUpdate()
        {
            rb.velocity = transform.up * speed;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Meteo"))
            {
                Destroy(gameObject);
                Destroy(other.gameObject);
            }
            else if (other.CompareTag("Enermy"))
            {
                Destroy(gameObject);
                Destroy(other.gameObject);
            }
            else if(other.CompareTag("EnermyGun"))
            {
                Destroy(gameObject);
                Destroy(other.gameObject);
            }
        }
    }
}
