﻿using System;
using UnityEngine;

namespace Project.Script.Player
{
    public class GunMissileMove : MonoBehaviour
    {
        [SerializeField] private float missileSpeed = 5f;
        [SerializeField] private float rotationSpeed;
        
        [SerializeField] private Transform target;
        [SerializeField] private Transform newTarget;
        
        private Rigidbody2D rb;

        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            
            Destroy(gameObject, 2f);
        }

        private void Update()
        {
            target = GameObject.FindGameObjectWithTag("Enermy").transform;
            if (target == null)
            {
                newTarget = GameObject.FindGameObjectWithTag("Enermy").transform;
                target = newTarget;
            }
        }

        void FixedUpdate()
        {
            MoveToEnermy();
        }
        
        private void MoveToEnermy()
        {
            Vector2 direction = (Vector2)target.position - rb.position;
            direction.Normalize();
            float rotationAmount = Vector3.Cross(direction, transform.up).z;
            rb.angularVelocity = -rotationAmount * rotationSpeed;

            rb.velocity = transform.up * missileSpeed;
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Meteo"))
            {
                Destroy(gameObject);
                Destroy(other.gameObject);
            }
            else if (other.CompareTag("Enermy"))
            {
                Destroy(gameObject);
                Destroy(other.gameObject);
            }
        }
    }
}
