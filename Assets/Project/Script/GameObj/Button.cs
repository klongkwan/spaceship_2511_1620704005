﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Project.Script.GameObj
{
    public class Button : MonoBehaviour
    {
        public void ButtonStart()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene("GameScene");
        }
        public void ButtonRetry()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene("GameScene");
        }
        public void ButtonResume()
        {
            Time.timeScale = 1;
        }
        public void ButtonPause()
        {
            Time.timeScale = 0;
        }
        public void ButtonHome()
        {
            Time.timeScale = 0;
            SceneManager.LoadScene("HomeScene");
        }
    }
}
