﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Project.Script.GameObj
{
    public class Detector : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("GunType"))
            {
                Destroy(other.gameObject);
            }
            else if (other.CompareTag("Meteo"))
            {
                Destroy(other.gameObject);
            }
            else if (other.CompareTag("EnermyGun"))
            {
                Destroy(other.gameObject);
            }
            else if (other.CompareTag("Enermy"))
            {
                Destroy(other.gameObject);
            }
            else if (other.CompareTag("Player"))
            {
                Time.timeScale = 0;
                SceneManager.LoadScene("GameOver");
            }
        }
    }
}
